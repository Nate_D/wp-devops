#!/bin/bash -e
clear
echo "============================================"
echo "WordPress Install Script"
echo "============================================"
echo "Do you need to setup new MySQL database? (y/n)"
read -e setupmysql
if [ "$setupmysql" == y ] ; then
	echo "MySQL Admin User: "
	read -e mysqluser
	echo "MySQL Admin Password: "
	read -s mysqlpass
	echo "MySQL Host (Enter for default 'localhost'): "
	read -e mysqlhost
		mysqlhost=${mysqlhost:-localhost}
fi
echo "WP Database Name: "
read -e dbname
echo "WP Database User: "
read -e dbuser
echo "WP Database Password: "
read -s dbpass
echo "WP Database Table (Enter for default 'wp_'): "
read -e dbtable
	dbtable=${dbtable:-wp_}
echo "Last chance - sure you want to run the install? (y/n)"
read -e run
if [ "$run" == y ] ; then
	if [ "$setupmysql" == y ] ; then
		echo "============================================"
		echo "Setting up the database."
		echo "============================================"
		#login to MySQL, add database, add user and grant permissions
		dbsetup="create database $dbname;GRANT ALL PRIVILEGES ON $dbname.* TO $dbuser@$mysqlhost IDENTIFIED BY '$dbpass';FLUSH PRIVILEGES;"
		mysql -u $mysqluser -p$mysqlpass -e "$dbsetup"
		if [ $? != "0" ]; then
			echo "============================================"
			echo "[Error]: Database creation failed. Aborting."
			echo "============================================"
			exit 1
		fi
	fi
	echo "============================================"
	echo "Installing WordPress for you."
	echo "============================================"
	#download wordpress
	echo "Downloading..."
	curl -O https://wordpress.org/latest.tar.gz
	#unzip wordpress
	echo "Unpacking..."
	tar -zxf latest.tar.gz
	#move /wordpress/* files to this dir
	echo "Moving..."
	mv wordpress/* ./
	echo "Configuring..."
	#create wp config
	cp wp-config-sample.php wp-config.php
	#set database details with perl find and replace
	perl -pi -e "s/database_name_here/$dbname/g" wp-config.php
	perl -pi -e "s/username_here/$dbuser/g" wp-config.php
	perl -pi -e "s/password_here/$dbpass/g" wp-config.php
	perl -pi -e "s/wp_/$dbtable/g" wp-config.php
	#create uploads folder and set permissions
	mkdir wp-content/uploads
	chmod 777 wp-content/uploads
	echo "Cleaning..."
	#remove wordpress/ dir
	rmdir wordpress
	#remove zip file
	rm latest.tar.gz
	#remove bash script if it exists in this dir
	[[ -f "$file" ]] && rm "wp.sh"
	echo "========================="
	echo "[Success]: Installation is complete."
	echo "========================="
else
	exit
fi